//DocumentLoaded
//Cuando mi javascript esta antes de crear algo en el HTML lo uso para que no me marque error
document.addEventListener('DOMContentLoaded',function(){
	console.log('El DOM ya fue construido!');
});

/* OBTENER ELEMENTOS POR ID */
const title = document.getElementById('title');
title.innerHTML = 'Cursos';//obtener o modificar el atributo de un elemento
//Modifico dichos id de mi HTML con javascript
const description = document.getElementById('description');
description.textContent = 'Listado de cursos';
console.log(description.textContent);
//description.innerHTML = '<strong>' + description.textContent+ '</strong>';


/* OBTENER ELEMENTOS POR CLASE 
//title.className en consola
//const items = document.getElementsByClassName('list-group-item');
for (var i = 0; i < items.length; i++) {
  let element = items[i];
  console.log(element);}
*/

/* OBTENER ELEMENTOS POR ETIQUETA 
//title.tagName
//const items = document.getElementsByTagName('li');
for (var i = 0; i < items.length; i++) {
	if(i%2 == 0){ //elementos en posicion par		
  		let element = items[i];
  		element.style.background = '#f2f2f2';
  	}
  	console.log(element);
}
*/

/* OBTENER ELEMENTOS CON REGLAS CSS
const element = document.querySelector('#first-course');
console.log(element);

const element2 = document.querySelector('.list-group-item');
console.log(element2);

const element3 = document.querySelector('li');
console.log(element3);
 */

/*querySelectorAll retorna un listado de elementos*/
/*Elementos situados en posicion impar; odd para posicion par*/
const items = document.querySelectorAll('li:nth-child(even)');
for (var i = 0; i < items.length; i++) {
  let element = items[i];
  element.style.background = '#f2f2f2';
}

/* ELEMENTOS HIJOS
const list = document.querySelector('ul');
console.log(list.childElementCount); //Cantidad de hijos que posee un elemento
console.log(list.children); //Obtener el listado de hijos

for (var i = 0; i < list.children.length; i++) {
  console.log(list.children[i]);
}
console.log(list.children[2]);
console.log(list.firstElementChild);
console.log(list.lastElementChild);
 */


 /* ELEMENTOS HERMANOS
const element = document.querySelector('div.row > ul.list-group > li');
console.log(element.parentElement); //Imprimo el elemento padre de este primer elemento
console.log(element.parentElement.parentElement); //Imprimo el elemento abuelo de li

console.log(element.nextElementSibling);//Imprimo al hermano; si no tiene es null
console.log(element.nextElementSibling.nextElementSibling);

const last_element = document.querySelector('last-course');
console.log(last_element);//EL ultimo hermano
console.log(last_element.previousElementSibling);//Retrocedo al hermano del ultimo elemento
 */


/* NODOS 
const element = document.getElementById('first-course');
console.log(element.childElementCount);
console.log(element.childNodes.length);
*/

/*===============================
=            EVENTOS            =
===============================*/
const button = document.querySelector('.btn.btn-primary');
button.addEventListener('click', function(){
	console.log('Un clic en button');
});
title.addEventListener('dblclick', function(){
	console.log('Doble clic');
});
//Argumentos: que tipo de evento le queremos colocar a nuestro elemento, una funcion


//target: elemento que dispara un evento
button.addEventListener('click', function(e){
	console.log(e);
	if(title.style.display != 'none'){
		title.style.display = 'none';
		description.style.display = 'none';
		//button.textContent = 'Mostrar';
		e.target.textContent = 'Mostrar';
	}else{
		title.style.display = 'block';
		description.style.display = 'block';
		//button.textContent = 'Ocultar';
		e.target.textContent = 'Ocultar';
	}
});

//Eventos del mouse
button.addEventListener('mouseenter', function() {
	this.className = 'btn btn-danger';	
});

button.addEventListener('mouseout', function() {
	this.className = 'btn btn-primary';	
});

//setTimeOut: funcion lineas de codigo pasado cierto tiempo
setTimeout(function(){
	console.log('Hola mundo');
},3000);
//Argumentos: una funcion y el tiempo en milisegundos que tiene que esperar la funcion para ser ejecutada

/* setTimeout(function(p1,p2,p3){
	console.log('Hola mundo');
},3000,,'argumento 1','argumento 2', 'argumento 3');
*/

//Eventos del teclado
const input = document.getElementById('input');
//este evento se va a disparar cada vez que presionemos una tecla del teclado
/*input.addEventListener('keydown', function(){
	console.log('Tecla presionada');
});*/
//saber que tecla fue presionada
input.addEventListener('keydown', function(e){
	console.log('Tecla presionada: ' + e.key + ' con un código ' + e.keyCode);
});

//Evento submit
const form = document.getElementById('course-form');
form.addEventListener('submit', function(e){
  e.preventDefault();

  let title = document.getElementById('title-form').value;//obtener el valor de un input con value
  let description = document.getElementById('description-form').value;
  console.log(title);
  console.log(description);
  //create_card(title, description);
});

//Eventos por cambio de valor
const checkbox = document.getElementById('checkbox');
checkbox.addEventListener('change', function(){
	console.log('Cambio de valor');
});

let title_form = document.getElementById('title-form');
title_form.addEventListener('change', function(){
	console.log('Cambio de valor');
});

//Propagacion de eventos Event building
/*const element = document.querySelector('li');
const list = document.querySelector('ul');
const div_row = document.querySelector('.row');
const div_container = document.querySelector('.container');
const body = document.querySelector('body');

/*element.addEventListener('click',function(){
	console.log('Elemento!');
});
list.addEventListener('click',function(){
	console.log('Lista!');
});
div_row.addEventListener('click',function(){
	console.log('Div Row!');
});
div_container.addEventListener('click',function(){
	console.log('Div Container!');
});
body.addEventListener('click',function(){
	console.log('Body!');
});

element.addEventListener('click', show_messages);
list.addEventListener('click', show_messages);
div_row.addEventListener('click', show_messages);
div_container.addEventListener('click', show_messages);
body.addEventListener('click', show_messages);
*/
//Lo mismo del bloque comentado con un for of
for(let element of document.querySelectorAll('*')){
  element.addEventListener('click', show_messages);
}
function show_messages(e){
	console.log("Elemento actual: " + this.tagName);//this hace referencia al elemento actual modificando su valor segun elemento se vaya propagando
  console.log("Elemento que disparo el evento: " + e.target.tagName);//el atributo target va a almacenar el elemento que disparo el evento
  console.log("\n");
}

/*PARA DETENER LA PROPAGACION USO e.stopPropagation();*/

/*=====  End of EVENTOS  ======*/

/*========================================
=            MODIFICAR EL DOM            =
========================================*/
//Crear,agregar y eliminar nuevos elementos
const row = document.querySelector('.row');

let div = null;

function create_card(title, description){

  div = document.createElement('div');
  div.className = 'col-sm-6 col-md-4';

  let thumbnail = document.createElement('div');
  thumbnail.className = 'thumbnail';

  let caption = document.createElement('div');
  caption.className = 'caption';

  let h3 = document.createElement('h3');
  h3.textContent = title;

  let p1 = document.createElement('p');
  p1.textContent = description;

  let p2 = document.createElement('p');
  let a = document.createElement('a');
  a.className = 'btn btn-danger';
  a.textContent = 'Eliminar';

  p2.addEventListener('click',delete_card);

  p2.appendChild(a);
  caption.appendChild(h3);
  caption.appendChild(p1);
  caption.appendChild(p2);

  thumbnail.appendChild(caption);
  div.appendChild(thumbnail);

  row.appendChild(div);
}

function delete_card(e){
  //El padre y el elemento a eliminar (hijo)
  let ancestor = get_ancestors(e.target, 4);
  row.removeChild(ancestor);
}

function get_ancestors(ancestor, level){
  if(level == 0){
    return ancestor;
  }
  level--;
  return get_ancestors(ancestor.parentElement, level);
}


function create_card_by_innerHTML(title, description){
  let html = `<div class="col-sm-6 col-md-4">\
                <div class="thumbnail">\
                  <div class="caption">\
                    <h3 id="title_card"> ${title} </h3>\
                    <p id="description_card"> ${description} </p>\
                    <p><a href="#" class="btn btn-danger">Acción</a></p>\
                  </div>\
                </div>\
              </div>`;
    row.innerHTML += html;
}

/*=====  End of MODIFICAR EL DOM  ======*/